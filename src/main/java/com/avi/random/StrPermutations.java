package com.avi.random;

public class StrPermutations {

    private void getPermutations(String input1) {
        permutate(input1, 0, input1.length() - 1);
    }

    private void permutate(String str, int left, int right) {
        if (left == right) {
            System.out.println(str);
        } else {
            for (int i = left; i <= right; i++) {
                str = swap(str, left, i);
                permutate(str, left + 1, right);
            }
        }
    }

    private String swap(String str, int left, int right) {
        char[] arr = str.toCharArray();
        char temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
        return String.valueOf(arr);
    }

    public static void main(String[] args) {
        String input1 = "abc";
        String input2 = "abcd";
        String input3 = "abcde";

        StrPermutations obj = new StrPermutations();
        obj.getPermutations(input2);
    }
}
