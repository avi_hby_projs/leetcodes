package com.avi.random;

import com.avi.Utils;

import java.util.*;

public class HighFive1086 {

    public int[][] highFive(int[][] items) {
        Arrays.sort(items, Comparator.comparingInt(i -> -i[1]));

        Map<Integer, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < items.length; i++) {
            if (map.containsKey(items[i][0])) {
                List<Integer> list = map.get(items[i][0]);
                if (list.size() < 6) {
                    list.add(items[i][1]);
                }
            } else {
                List<Integer> list = new ArrayList<>();
                list.add(items[i][1]);
                map.put(items[i][0], list);
            }
        }

        int[][] result = new int[map.size()][];

        int idx = 0;
        for (Integer i : map.keySet()) {
            List<Integer> list = map.get(i);
            int sum = 0;
            for (int j = 0; j < 5; j++) {
                sum = sum + list.get(i);
            }
            int[] t = new int[2];
            t[0] = i;
            t[1] = sum / 5;
            result[idx] = t;
            ++idx;
        }


        return result;
    }

    public static void main(String[] args) {
        int[][] input1 = {{1, 91}, {1, 92}, {2, 93}, {2, 97}, {1, 60}, {2, 77}, {1, 65}, {1, 87}, {1, 100}, {2, 100}, {2, 76}};
        int[][] input2 = {{1, 84}, {1, 72}, {1, 47}, {1, 43}, {1, 78}, {2, 79}, {2, 4}, {2, 23}, {2, 88}, {2, 79}, {3, 75}, {3, 80}, {3, 38}, {3, 73}, {3, 4}};

        HighFive1086 hf = new HighFive1086();
        int[][] result = hf.highFive(input2);
        Utils.printArray(result);
    }
}
