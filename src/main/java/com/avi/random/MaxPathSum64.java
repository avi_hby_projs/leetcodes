package com.avi.random;

import java.util.ArrayList;
import java.util.List;

import static com.avi.Utils.printArray;
import static com.avi.Utils.printListsOfList;

public class MaxPathSum64 {

    public List<List<String>> maxPath(int[][] grid) {
        int rows = grid.length;
        int cols = grid[0].length;
        int[][] sumArray = new int[rows][cols];
        List<List<String>> path = new ArrayList<>();

        for (int i = rows - 1; i >= 0; i--) {
            for (int j = 0; j < cols; j++) {
                if (i + 1 == rows && j - 1 < 0) {
                    sumArray[i][j] = grid[i][j];
                } else if (i + 1 == rows) {
                    sumArray[i][j] = sumArray[i][j - 1] + grid[i][j];
                } else if (j - 1 < 0) {
                    sumArray[i][j] = sumArray[i + 1][j] + grid[i][j];
                } else {
                    sumArray[i][j] = Math.max(sumArray[i][j - 1], sumArray[i + 1][j]) + grid[i][j];
                }
            }
        }

        int i = rows - 1;
        int j = 0;
        List<String> temp = new ArrayList<>();
        temp.add(String.valueOf(i));
        temp.add(String.valueOf(j));
        path.add(temp);

        while (i != 0 && j != cols - 1) {
            if (i - 1 == -1) {
                j = j + 1;
            } else if (j + 1 == cols) {
                i = i - 1;
            } else {
                int right = sumArray[i][j + 1];
                int up = sumArray[i - 1][j];
                if (up > right) {
                    i = i - 1;
                } else {
                    j = j + 1;
                }
            }

            temp = new ArrayList<>();
            temp.add(String.valueOf(i));
            temp.add(String.valueOf(j));
            path.add(temp);
        }


        return path;
    }

    public static void main(String[] args) {
        int[][] grid1 = {{1, 3, 1}, {1, 5, 1}, {4, 2, 1}};
        int[][] grid2 = {{1, 2, 3}, {4, 5, 6}};

        MaxPathSum64 minPath = new MaxPathSum64();
        List<List<String>> result = minPath.maxPath(grid1);
        printListsOfList(result);
    }

}
