package com.avi;

import java.util.List;

public class Utils {

    public static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; ++i) {
            for (int j = 0; j < arr[i].length; ++j) {
                System.out.print(arr[i][j] + ", ");
            }
            System.out.println();
        }
    }

    public static void printArray(int[] arr) {
        System.out.print("[ ");
        for (int i = 0; i < arr.length; ++i) {
            System.out.print(arr[i]);
            if (i != arr.length - 1)
                System.out.print(", ");
        }
        System.out.print(" ]");
        System.out.println();
    }

    public static void printArray(Integer[] arr) {
        System.out.print("[ ");
        for (int i = 0; i < arr.length; ++i) {
            System.out.print(arr[i]);
            if (i != arr.length - 1)
                System.out.print(", ");
        }
        System.out.print(" ]");
        System.out.println();
    }

    public static void printList(List<Integer> list) {
        if (list == null)
            return;

        System.out.print("[ ");
        for (int i = 0; i < list.size(); ++i) {
            System.out.print(list.get(i));
            if (i != list.size() - 1)
                System.out.print(", ");
        }
        System.out.print(" ]");
        System.out.println();
    }

    public static void printListsOfList(List<List<String>> list) {
        if (list == null)
            return;

        System.out.print("[ ");
        for (int i = 0; i < list.size(); ++i) {
            List<String> s = list.get(i);
            System.out.print("[ ");
            for (int j = 0; j < s.size(); j++) {
                System.out.print(s.get(j));
                if (j != s.size() - 1)
                    System.out.print(", ");
            }
            System.out.print(" ]");
            if (i != list.size() - 1)
                System.out.print(", ");
        }
        System.out.print(" ]");
        System.out.println();
    }
}
