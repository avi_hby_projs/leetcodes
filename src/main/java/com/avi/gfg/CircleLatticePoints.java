package com.avi.gfg;

import java.util.ArrayList;
import java.util.List;

import static com.avi.Utils.printArray;

// https://www.geeksforgeeks.org/circle-lattice-points/
public class CircleLatticePoints {

    public List<Integer> getCircleLatticePoints(int radius) {
        List<Integer> latticePoints = new ArrayList<>();

        latticePoints.add(0);
        latticePoints.add(radius);
        latticePoints.add(radius);
        latticePoints.add(0);

        searchLatticePoints(latticePoints, radius);
        generateCombinations(latticePoints);

        return latticePoints;
    }

    private void generateCombinations(List<Integer> latticePoints) {
        int len = latticePoints.size();

        for (int i = 0; i < len; i = i + 2) {
            int x = latticePoints.get(i);
            int y = latticePoints.get(i + 1);
            if (x != 0) {
                latticePoints.add(-1 * x);
                latticePoints.add(y);
            }
            if (y != 0) {
                latticePoints.add(x);
                latticePoints.add(-1 * y);
            }
            if (x != 0 && y != 0) {
                latticePoints.add(-1 * x);
                latticePoints.add(-1 * y);
            }
        }
    }

    private void searchLatticePoints(List<Integer> latticePoints, int radius) {

        for (int i = 1; i < radius; ++i) {
            float y = (float) Math.sqrt(radius * radius - i * i);
            if (y % 1 == 0) {
                latticePoints.add(i);
                latticePoints.add((int) y);
            }
        }
    }


    public static void main(String[] args) {
        int radius = 5;
        CircleLatticePoints clp = new CircleLatticePoints();
        List<Integer> latticePts = clp.getCircleLatticePoints(radius);
        System.out.println("Number of lattice points: " + latticePts.size() / 2);
        printArrayList(latticePts);
    }

    public static void printArrayList(List<Integer> latticePts) {
        for (int i = 0; i < latticePts.size(); i = i + 2) {
            System.out.println("(" + latticePts.get(i) + ", " + latticePts.get(i + 1) + ")");
        }
    }

}
