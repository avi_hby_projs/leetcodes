package com.avi.gfg;

import java.util.Arrays;
import java.util.Comparator;

import static com.avi.Utils.printArray;

// https://www.geeksforgeeks.org/sort-even-placed-elements-increasing-odd-placed-decreasing-order/
public class AscDescArray {

    private static Integer[] ascDscArray(int[] input1) {
        int len = input1.length;
        Integer[] op = new Integer[len];

        // Fill even items
        int outputIndex = 0;
        for (int i = 0; i < len; i = i + 2) {
            op[outputIndex] = input1[i];
            ++outputIndex;
        }

        // Fill odd items
        for (int i = 1; i < len; i = i + 2) {
            op[outputIndex] = input1[i];
            ++outputIndex;
        }

        int mid = len / 2;
        if (len % 2 != 0)
            ++mid;

        Arrays.sort(op, 0, mid);
        Arrays.sort(op, mid, len, Comparator.reverseOrder());
        return op;
    }

    public static void main(String[] args) {
        int[] input1 = new int[]{0, 1, 2, 3, 4, 5, 6, 7};
        int[] input2 = new int[]{3, 1, 2, 4, 5, 9, 13, 14, 12};
        Integer[] outPut = ascDscArray(input2);
        printArray(outPut);
    }
}
