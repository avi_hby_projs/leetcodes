package com.avi.gfg;

public class DetectConsecSeqInString {

    private boolean hasSequence(String inpStr) {
        boolean result = false;
        int strLen = inpStr.length();
        int maxLen = strLen / 2;

        for (int i = 1; i < maxLen; i++) {
            int nextExpectedNum = 0;
            int num = 0;

            // Set sliding window parameters
            int start = 0;
            int end = i;
            while (end < strLen) {
                num = getNumFromString(inpStr, start, end);
                if (start != 0 && nextExpectedNum != num) {
                    result = false;
                    break;
                }
                nextExpectedNum = num + 1;
                start = end;
                // Next number will be of one higher digit if orignal number is all 9 (9, 99, 999, etc)
                end = start + getNumofDigits(nextExpectedNum);
                result = true;
            }
            if (result)
                break;
        }

        return result;
    }

    private int getNumofDigits(int nextExpectedNum) {
        String numStr = String.valueOf(nextExpectedNum);
        return numStr.length();
    }

    private int getNumFromString(String str, int j, int i) {
        String subs = str.substring(j, i);
        return Integer.parseInt(subs);
    }

    public static void main(String[] args) {
        String inp1 = "99100101";
        String inp2 = "991010";

        DetectConsecSeqInString obj = new DetectConsecSeqInString();
        boolean result = obj.hasSequence(inp2);
        System.out.println("Has Sequence: " + result);
    }
}
