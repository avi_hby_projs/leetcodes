package com.avi.leetcode;

public class WalkingRobotSim874 {

    public int robotSim(int[] commands, int[][] obstacles) {
        int farthestDist = 0;
        int currDirectionIdx = 0;
        int[] currPosition = new int[]{0, 0};
        int[][] directionVector = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

        for (int i = 0; i < commands.length; i++) {
            if (commands[i] == -1) {
                currDirectionIdx++;
                currDirectionIdx = currDirectionIdx % 4;
            } else if (commands[i] == -2) {
                currDirectionIdx--;
                currDirectionIdx = currDirectionIdx < 0 ? 3 : currDirectionIdx;
            } else {
                int currX = currPosition[0];
                int currY = currPosition[1];
                int newX = currX + commands[i] * directionVector[currDirectionIdx][0];
                int newY = currY + commands[i] * directionVector[currDirectionIdx][1];

                // Obstacle processing
                for (int j = 0; j < obstacles.length; j++) {
                    int obstacleX = obstacles[j][0];
                    int obstacleY = obstacles[j][1];
                    // We are moving in X direction and we are in line of an obstacle
                    if (currY == newY && currY == obstacleY) {
                        // We are moving in East
                        if (currDirectionIdx == 1 && (currX < obstacleX && newX >= obstacleX)) {
                            newX = obstacleX - 1;
                        }
                        // We are moving in West
                        if (currDirectionIdx == 3 && (currX > obstacleX && newX <= obstacleX)) {
                            newX = obstacleX + 1;
                        }
                    } else if (currX == newX && currX == obstacleX) { // We are moving in Y direction and we are in line of an obstacle
                        // We are moving in North
                        if (currDirectionIdx == 0 && (currY < obstacleY && newY >= obstacleY)) {
                            newY = obstacleY - 1;
                        }
                        // We are moving in South
                        if (currDirectionIdx == 2 && (currY > obstacleY && newY <= obstacleY)) {
                            newY = obstacleY + 1;
                        }
                    }
                }
                currPosition[0] = newX;
                currPosition[1] = newY;
                int eucledianDistance = currPosition[0] * currPosition[0] + currPosition[1] * currPosition[1];
                if (eucledianDistance > farthestDist)
                    farthestDist = eucledianDistance;
            }
        }

        return farthestDist;
    }

    public static void main(String[] args) {
        int[] commands1 = new int[]{4, -1, 4, -2, 4};
        int[][] obstacles1 = new int[][]{{2, 4}};
        int[] commands2 = new int[]{6, -1, -1, 6};
        int[][] obstacles2 = new int[0][0];

        WalkingRobotSim874 sim = new WalkingRobotSim874();
        int result = sim.robotSim(commands2, obstacles2);
        System.out.println("Euclidean Distance: " + result);
    }

}
