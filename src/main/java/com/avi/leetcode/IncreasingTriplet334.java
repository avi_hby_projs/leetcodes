package com.avi.leetcode;

public class IncreasingTriplet334 {

    public boolean increasingTriplet(int[] nums) {
        long i = Long.MIN_VALUE;
        long j = Long.MIN_VALUE;
        long k = Long.MIN_VALUE;

        for (int l = 0; l < nums.length; l++) {
            if (i == Long.MIN_VALUE || nums[l] <= i) {
                i = nums[l];
            } else if (j == Long.MIN_VALUE || nums[l] <= j) {
                j = nums[l];
            } else if (k == Long.MIN_VALUE || nums[l] <= k) {
                return true;
            }
        }

        return false;
    }


    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 4, 5};
        int[] nums2 = {2, 1, 5, 0, 4, 6};
        int[] nums3 = {20, 100, 10, 12, 5, 13};
        int[] nums4 = {1, 1, 1, 1, 1, 1};
        int[] nums5 = {0, 4, 1, -1, 2};

        IncreasingTriplet334 obj = new IncreasingTriplet334();
        boolean result = obj.increasingTriplet(nums5);
        System.out.println("Increasing Triplets: " + result);
    }

}
