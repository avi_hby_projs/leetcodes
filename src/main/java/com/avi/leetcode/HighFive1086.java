package com.avi.leetcode;

import com.avi.Utils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class HighFive1086 {

    public int[][] highFive(int[][] items) {
        Arrays.sort(items, Comparator.comparingInt(i -> -i[1]));

        Map<Integer, Integer[]> resultMap = new HashMap<>();

        for (int i = 0; i < items.length; ++i) {
            if (resultMap.containsKey(items[i][0])) {
                Integer[] value = resultMap.get(items[i][0]);
                int idx = value[0];
                ++idx;
                if (idx < 6) {
                    value[idx] = items[i][1];
                    value[0] = idx;
                }
            } else {
                resultMap.put(items[i][0], new Integer[]{1, items[i][1], 0, 0, 0, 0});
            }
        }

        int[][] result = new int[resultMap.size()][];
        // Consolidate result array
        int index = 0;
        for (Integer i : resultMap.keySet()) {
            Integer[] data = resultMap.get(i);
            int[] avg = new int[2];
            avg[0] = i;
            avg[1] = (data[1] + data[2] + data[3] + data[4] + data[5]) / 5;
            result[index] = avg;
            ++index;
        }

        Arrays.sort(result, Comparator.comparingInt(i -> i[0]));

        return result;
    }

    public static void main(String[] args) {
        int[][] input1 = {{1, 91}, {1, 92}, {2, 93}, {2, 97}, {1, 60}, {2, 77}, {1, 65}, {1, 87}, {1, 100}, {2, 100}, {2, 76}};
        int[][] input2 = {{1, 84}, {1, 72}, {1, 47}, {1, 43}, {1, 78}, {2, 79}, {2, 4}, {2, 23}, {2, 88}, {2, 79}, {3, 75}, {3, 80}, {3, 38}, {3, 73}, {3, 4}};

        HighFive1086 hf = new HighFive1086();
        int[][] result = hf.highFive(input2);
        Utils.printArray(result);
    }
}
