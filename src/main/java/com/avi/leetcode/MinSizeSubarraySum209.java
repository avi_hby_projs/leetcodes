package com.avi.leetcode;

import java.util.List;

import static com.avi.Utils.printListsOfList;

public class MinSizeSubarraySum209 {
    public int minSubArrayLen(int target, int[] nums) {
        int i = 0;
        int j = 0;
        int sum = 0;
        int minLength = -1;
        int count = 0;

        while (i < nums.length) {
            sum = sum + nums[j];
            count++;
            if (sum >= target) {
                if (count < minLength || minLength == -1) {
                    minLength = count;
                }
                i++;
                j = i;
                count = 0;
                sum = 0;
            } else {
                j++;
                if (j == nums.length) {
                    i++;
                    j = i;
                    count = 0;
                    sum = 0;
                }
            }
        }

        return minLength == -1 ? 0 : minLength;
    }

    public static void main(String[] args) {
        int target1 = 7;
        int[] nums1 = {2, 3, 1, 2, 4, 3};

        MinSizeSubarraySum209 obj = new MinSizeSubarraySum209();
        int result = obj.minSubArrayLen(target1, nums1);
        System.out.println("Result: " + result);
    }

}
