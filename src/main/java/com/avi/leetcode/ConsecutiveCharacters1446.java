package com.avi.leetcode;

public class ConsecutiveCharacters1446 {

    public int maxPower(String s) {
        int result = 0;
        int count = 1;
        int i = 0;
        int j = 1;

        if (s.length() == 1)
            return count;

        while (j < s.length()) {
            if (s.charAt(i) == s.charAt(j)) {
                count++;
                j++;
            } else {
                if (count > result)
                    result = count;
                count = 1;
                i = j;
                j = j + 1;
            }
        }

        if (count > result)
            result = count;

        return result;
    }

    public static void main(String[] args) {
        String input1 = "abbcccddddeeeeedcba";
        String input2 = "cc";
        ConsecutiveCharacters1446 cc = new ConsecutiveCharacters1446();
        int result = cc.maxPower(input2);
        System.out.println("Output: " + result);
    }
}
