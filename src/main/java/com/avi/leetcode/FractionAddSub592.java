package com.avi.leetcode;

public class FractionAddSub592 {
    public String fractionAddition(String expression) {
        String[] nums = expression.replace("-", "#-").replace("+", "#").split("[#/]");
        int startIdx = nums[0].length() == 0 ? 1 : 0;
        int N1 = Integer.valueOf(nums[startIdx]);
        int D1 = Integer.valueOf(nums[startIdx + 1]);

        for (int i = startIdx + 2; i < nums.length; i = i + 2) {
            int N2 = Integer.valueOf(nums[i]);
            int D2 = Integer.valueOf(nums[i + 1]);
            int lcm = getLCM(D1, D2);
            int RN = lcm / D1 * N1 + lcm / D2 * N2;
            int RD = lcm;
            N1 = RN;
            D1 = RD;
        }
        if (N1 != 0) {
            int gcd = getGCD(N1, D1);
            N1 = N1 / gcd;
            D1 = D1 / gcd;
        } else {
            D1 = 1;
        }

        return N1 + "/" + D1;
    }

    private int getLCM(int d1, int d2) {
        return Math.abs(d1 * d2) / getGCD(d1, d2);
    }

    private int getGCD(int d1, int d2) {
        int a = Math.abs(d1);
        int b = Math.abs(d2);
        while (b != 0) {
            int min = Math.min(a, b);
            int max = Math.max(a, b);
            b = max % min;
            a = min;
        }
        return a;
    }

    public static void main(String[] args) {
        String expression1 = "-1/2+1/2+1/3";
        String expression2 = "-1/2+1/2-1/3";
        String expression3 = "-1/2+1/2";
        String expression4 = "1/3-1/2";

        FractionAddSub592 obj = new FractionAddSub592();
        String result = obj.fractionAddition(expression4);
        System.out.println("Result: " + result);
    }

}
