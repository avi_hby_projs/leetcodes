package com.avi.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class JosepheusProb1823 {

    public int findTheWinner(int n, int k) {

        Queue<Integer> q = new LinkedList<>();

        for (int i = 1; i <= n; ++i) {
            q.offer(i);
        }

        while (q.size() != 1) {
            for (int i = 1; i <= k - 1; ++i) {
                Integer p = q.poll();
                q.offer(p);
            }
            q.poll();
        }

        return q.poll();
    }

    public static void main(String[] args) {
        int n1 = 6;
        int k1 = 5;

        JosepheusProb1823 obj = new JosepheusProb1823();
        int result = obj.findTheWinner(n1, k1);
        System.out.println("Last Winner: " + result);
    }
}
