package com.avi.leetcode;

import java.util.Arrays;

public class MeetingRoomsII253 {

    public int meetingRoom(int[][] input) {
        // See Neetcode explaination: https://www.youtube.com/watch?v=FdzJmTCVyJU
        int len = input.length;
        int[] startArr = new int[len];
        int[] endArr = new int[len];

        // Put start and end time in different arrays
        for (int i = 0; i < len; i++) {
            startArr[i] = input[i][0];
            endArr[i] = input[i][1];
        }

        Arrays.sort(startArr);
        Arrays.sort(endArr);

        int s = 0;
        int e = 0;
        int count = 0;
        int max = 0;

        // We just need to only parse complete start time array as this will give as max meeting count. The
        // end time array will just decrease its count.
        while (s < len) {
            // Compare start and end time
            // If start time < end time then one meeting has started, this is stored in count
            if (startArr[s] < endArr[e]) {
                // Increment meeting count and increase start time index with one. End time index remains same
                count++;
                s++;
            } else {
                // If end time >= start time then one meeting has ended, so decrement the count and exceed end
                // array index. start time index remains same
                count--;
                e++;
            }
            // Store the max value count has ever taken
            max = count > max ? count : max;
        }

        return max;
    }

    public static void main(String[] args) {
        int[][] input1 = {{0, 30}, {10, 15}, {20, 30}};
        int[][] input2 = {{0, 30}, {10, 20}, {20, 30}};
        int[][] input3 = {{0, 30}, {10, 25}, {20, 30}};

        MeetingRoomsII253 obj = new MeetingRoomsII253();
        int result = obj.meetingRoom(input3);
        System.out.println("Rooms: " + result);
    }

}
