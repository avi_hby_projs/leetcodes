package com.avi.leetcode;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class WordSearchII212 {
    // Notes:
    // 1) It is solved by a combination of DFS backtracking and Trie data structure.
    // 2) Good resource for learning Trie data structure:
    //    https://medium.com/basecs/trying-to-understand-tries-3ec6bede0014
    // 3) How to reset StringBuilder?
    //    sb.setLength(0);
    // 4) How to remove last character in StringBuilder?
    //    sb.setLength(Math.max(sb.length() - 1, 0));
    // 5) How to make List from Set?
    //    list.addAll(set);
    // 6) In this solution visitedSet is removed to avoid 'Time Limit Exceeded'.
    //    The visited location is set to '#' to mark it as visited. This optimizes solution to great extent.
    // 7) A single location on the grid can be the starting letter of many words, for e.g. "hklf", "hf", hfk. So,
    //    it is important to save the word and then continue the search for further possible words. Also, explore
    //    all four adjoining coordinates (this approach is different from WordSearch79 where we return true after
    //    first find).
    public List<String> findWords(char[][] board, String[] words) {
        List<String> wordList = new LinkedList<>();
        TrieNode node = createTrie(words);
        Set<String> wordSet = new HashSet<>();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                char currChar = board[i][j];
                if (node.nodeArr[currChar - 'a'] != null) {
                    StringBuilder sb = new StringBuilder();
                    backtrackingDFS(board, i, j, node, sb, wordSet);
                }
            }
        }

        wordList.addAll(wordSet);
        return wordList;
    }

    private boolean backtrackingDFS(char[][] board, int i, int j, TrieNode node, StringBuilder sb,
                                    Set<String> wordSet) {
        // Check invalid coordinates or already visited coordinate
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length || board[i][j] == '#') {
            return false;
        }

        char currChar = board[i][j];

        if (node.nodeArr[currChar - 'a'] == null) {
            return false;
        }

        // Matched current char
        sb.append(currChar);
        board[i][j] = '#';

        TrieNode nextNode = node.nodeArr[currChar - 'a'];
        if (nextNode.wordEnd) {
            // Set the word to false as we don't want to detect
            // this word more than once.
            nextNode.wordEnd = false;
            wordSet.add(sb.toString());
        }

        backtrackingDFS(board, i + 1, j, nextNode, sb, wordSet);
        backtrackingDFS(board, i, j + 1, nextNode, sb, wordSet);
        backtrackingDFS(board, i - 1, j, nextNode, sb, wordSet);
        backtrackingDFS(board, i, j - 1, nextNode, sb, wordSet);

        board[i][j] = currChar;
        sb.setLength(Math.max(sb.length() - 1, 0));

        return false;
    }

    private TrieNode createTrie(String[] words) {
        TrieNode root = new TrieNode();

        for (int i = 0; i < words.length; i++) {
            TrieNode currNode = root;
            for (int j = 0; j < words[i].length(); j++) {
                char currCh = words[i].charAt(j);
                if (currNode.nodeArr[currCh - 'a'] == null) {
                    TrieNode newNode = new TrieNode();
                    currNode.nodeArr[currCh - 'a'] = newNode;
                    currNode = newNode;
                } else {
                    currNode = currNode.nodeArr[currCh - 'a'];
                }
                if (j == words[i].length() - 1) {
                    currNode.wordEnd = true;
                }
            }
        }

        return root;
    }

    class TrieNode {
        public boolean wordEnd;
        public TrieNode[] nodeArr;

        TrieNode() {
            this.wordEnd = false;
            this.nodeArr = new TrieNode[26];
        }
    }

    public static void main(String[] args) {
        WordSearchII212 obj = new WordSearchII212();
        char[][] board = {{'o', 'a', 'a', 'n'}, {'e', 't', 'a', 'e'}, {'i', 'h', 'k', 'r'}, {'i', 'f', 'l', 'v'}};
        String[] word = {"oath", "pea", "eat", "rain"};
        System.out.println("Word Present: " + obj.findWords(board, word)); // Output: ["eat","oath"]

        board = new char[][]{{'o', 'a', 'a', 'n'}, {'e', 't', 'a', 'e'}, {'i', 'h', 'k', 'r'}, {'i', 'f', 'l', 'v'}};
        word = new String[]{"oath", "pea", "eat", "rain", "hklf", "hf"};
        System.out.println("Word Present: " + obj.findWords(board, word)); // Output: ["oath","eat","hklf","hf"]
    }
}
