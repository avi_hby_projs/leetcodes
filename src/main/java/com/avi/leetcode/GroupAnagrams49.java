package com.avi.leetcode;

import java.util.*;

import static com.avi.Utils.printListsOfList;

public class GroupAnagrams49 {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> result = new LinkedList<>();
        Map<String, List<String>> dataMap = new HashMap<>();

        for (int i = 0; i < strs.length; ++i) {
            String item = strs[i];
            String itemSorted = sortedStr(item);
            if (dataMap.containsKey(itemSorted)) {
                dataMap.get(itemSorted).add(item);
            } else {
                List<String> l = new LinkedList<String>();
                l.add(item);
                dataMap.put(itemSorted, l);
            }
        }

        for (String k : dataMap.keySet()) {
            List<String> i = dataMap.get(k);
            result.add(i);
        }
        // Collections.sort(result, Comparator.comparingInt(i -> i.size()));

        return result;
    }

    private String sortedStr(String item) {
        char[] arr = item.toCharArray();
        Arrays.sort(arr);
        return new String(arr);
    }

    public static void main(String[] args) {
        String[] strs1 = {"eat", "tea", "tan", "ate", "nat", "bat"};

        GroupAnagrams49 obj = new GroupAnagrams49();
        List<List<String>> result = obj.groupAnagrams(strs1);
        printListsOfList(result);
    }
}
