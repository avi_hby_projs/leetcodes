package com.avi.leetcode;

import java.util.Arrays;

public class LargestNumber179 {

    public String getlargestNumber(int[] nums) {
        String[] a = Arrays.stream(nums).mapToObj(String::valueOf).toArray(String[]::new);
        Arrays.sort(a, (i, j) -> Long.valueOf(j + i).compareTo(Long.valueOf(i + j)));
        String c = Arrays.stream(a).reduce("", String::concat);
        int len = c.length();
        for (int i = 0; i < len; i++) {
            if (c.charAt(i) != '0')
                break;
            return "0";
        }

        return c;
    }

    public static void main(String[] args) {
        int[] input = new int[]{3, 30, 34, 5, 9};
        int[] input1 = new int[]{999999991, 9};
        int[] input2 = new int[]{999999998,999999997,999999999};
        LargestNumber179 lm = new LargestNumber179();
        String largestNum = lm.getlargestNumber(input2);
        System.out.println("Largest Number: " + largestNum);
    }
}
