package com.avi.leetcode;

import java.util.*;

public class MaxEvents1353 {
    public int maxEvents(int[][] events) {
        // See explaination: https://leetcode.ca/2019-08-14-1353-Maximum-Number-of-Events-That-Can-Be-Attended/
        // Go through each day (from first StartDay to the last EndDay) and pick
        // smallest EndDay, and attend that event first (i.e. increment event count),
        // since, this event will end first. Then attend event with next smallest
        // EndDay and so on.
        int startDay = Integer.MAX_VALUE;
        int endDay = -1;
        Map<Integer, List<Integer>> map = new HashMap<>();
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        int numEvents = 0;

        // Create a Map StartDay -> [List of EndDays]
        for (int i = 0; i < events.length; i++) {
            List<Integer> list = map.getOrDefault(events[i][0], new LinkedList<>());
            list.add(events[i][1]);
            if (!map.containsKey(events[i][0]))
                map.put(events[i][0], list);
            // Capture StartDay and EndDay for the whole input array
            startDay = Math.min(startDay, events[i][0]);
            endDay = Math.max(endDay, events[i][1]);
        }

        // Go through each day and remove smallest EndDay and increment
        // MaxEvents
        for (int i = startDay; i <= endDay; i++) {
            // Remove all endDays < startDay as this is invalid condition
            while (!pq.isEmpty() && pq.peek() < i) {
                pq.poll();
            }

            // Add the whole list to PriorityQueue
            List<Integer> list = map.getOrDefault(i, new LinkedList<>());
            for (Integer eDays : list) {
                pq.offer(eDays);
            }

            // Pick and remove smallest EndDay and increment event count
            if (!pq.isEmpty()) {
                pq.poll();
                numEvents++;
            }
        }

        return numEvents;
    }

    public static void main(String[] args) {
        MaxEvents1353 obj = new MaxEvents1353();

        int[][] events = {{1, 2}, {2, 3}, {3, 4}};
        System.out.println("Result: " + obj.maxEvents(events)); // Output: 3

        events = new int[][]{{1, 2}, {2, 3}, {3, 4}, {1, 2}};
        System.out.println("Result: " + obj.maxEvents(events)); // Output: 4

        events = new int[][]{{1, 2}, {1, 2}, {1, 6}, {1, 2}, {1, 2}};
        System.out.println("Result: " + obj.maxEvents(events)); // Output: 3
    }
}
