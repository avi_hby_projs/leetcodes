package com.avi.leetcode;

import java.util.Arrays;

public class Candy135 {
    public int candy(int[] ratings) {
        // Neetcode explaination: https://www.youtube.com/watch?v=1IzCRCcK17A
        int len = ratings.length;
        int[] candyArr = new int[len];
        Arrays.fill(candyArr, 1);

        for (int i = 1; i < len; i++) {
            if (ratings[i] > ratings[i - 1] && candyArr[i] <= candyArr[i - 1])
                candyArr[i] = candyArr[i - 1] + 1;
        }

        for (int i = len - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1] && candyArr[i] <= candyArr[i + 1])
                candyArr[i] = candyArr[i + 1] + 1;
        }
        int result = 0;
        for (int i = 0; i < len; i++) {
            result = result + candyArr[i];
        }
        return result;
    }

    public static void main(String[] args) {
        int[] ratings1 = {1, 2, 87, 87, 87, 2, 1};

        Candy135 obj = new Candy135();
        int result = obj.candy(ratings1);
        System.out.println("Result: " + result);
    }
}
