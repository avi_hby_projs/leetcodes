package com.avi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class MaxKSumPairs1679 {

    public int maxOperations(int[] nums, int k) {
        int result = 0;

        Map<Integer, Integer> countMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            countMap.put(nums[i], countMap.getOrDefault(nums[i], 0) + 1);
        }

        for (Integer i : countMap.keySet()) {
            // Self match
            if (countMap.get(i) != 0) {
                if (i * 2 == k) {
                    int count = countMap.get(i);
                    int pairs = count / 2;
                    int leftCount = count - 2 * pairs;
                    countMap.put(i, leftCount);
                    result = result + pairs;
                } else {
                    int otherNum = k - i;
                    if (countMap.containsKey(otherNum) && countMap.get(otherNum) != 0) {
                        int countFirst = countMap.get(i);
                        int countSec = countMap.get(otherNum);
                        int pairs = Math.min(countFirst, countSec);
                        countFirst = countFirst - pairs;
                        countSec = countSec - pairs;
                        countMap.put(i, countFirst);
                        countMap.put(otherNum, countSec);
                        result = result + pairs;
                    }
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 4};
        int k1 = 5;
        int[] nums2 = {3, 1, 3, 4, 3};
        int k2 = 6;

        MaxKSumPairs1679 obj = new MaxKSumPairs1679();
        int result = obj.maxOperations(nums2, k2);
        System.out.println("Max K Sum pairs: " + result);
    }

}
