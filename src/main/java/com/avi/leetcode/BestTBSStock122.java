package com.avi.leetcode;

import com.avi.Utils;

import java.util.Arrays;

public class BestTBSStock122 {

    public int maxProfit(int[] prices) {

        int cumulativeProfit = 0;
        int buyPrice = prices[0];

        // Traverse through the array and keep on accumulating profits
        for (int i = 1; i < prices.length; i++) {
            int curProfit = prices[i] - buyPrice;
            if (curProfit > 0) {
                cumulativeProfit = cumulativeProfit + curProfit;
            }

            buyPrice = prices[i];
        }

        return cumulativeProfit;
    }

    public static void main(String[] args) {
        int[] prices1 = {7, 1, 5, 3, 6, 4};
        int[] prices2 = {7, 6, 4, 3, 1};

        BestTBSStock122 obj = new BestTBSStock122();
        int result = obj.maxProfit(prices2);
        System.out.println("Result: " + result);
    }
}
