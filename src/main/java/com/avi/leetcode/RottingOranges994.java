package com.avi.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class RottingOranges994 {
    public int orangesRotting(int[][] grid) {
        // See Neetcode explaination: https://www.youtube.com/watch?v=y704fEOx0s0
        // Here we need to do BFS from multiple rotten oranges at the same time so
        // normal BFS will not work. First put all the rotten oranges in queue to
        // perform multisource BFS. Increment time for each cycle of BFS to get
        // the number of days.
        int freshOranges = 0;
        Queue<String> q = new LinkedList<>();

        // Count the number of fresh oranges and put all the rotten oranges in
        // queue for performing multisource BFS
        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[0].length; c++) {
                if (grid[r][c] == 1) {
                    freshOranges++;
                } else if (grid[r][c] == 2) {
                    q.offer(r + "_" + c);
                }
            }
        }

        // Perform multisource BFS
        int[] result = multiSourceBfs(grid, q);

        // If there are leftover fresh oranges then return -1
        if (result[1] < freshOranges)
            return -1;

        return result[0];
    }

    private int[] multiSourceBfs(int[][] grid, Queue<String> q) {
        int[] result = new int[2];
        int convertedOranges = 0;
        int time = 0;
        int[][] direcVector = {
                {0, 1},
                {1, 0},
                {0, -1},
                {-1, 0}};

        if (q.isEmpty()) {
            result[0] = time;
            result[1] = convertedOranges;
            return result;
        }

        while (!q.isEmpty()) {
            int qLen = q.size();
            time++;
            // Run for the set of queue element
            for (int l = 1; l <= qLen; l++) {
                String curr = q.poll();
                int[] currLoc = getLocFromStr(curr);

                for (int i = 0; i < 4; i++) {
                    int newR = currLoc[0] + direcVector[i][0];
                    int newC = currLoc[1] + direcVector[i][1];
                    // Put into queue if coordinates are valid and
                    // orange is not rotten
                    if (isValid(newR, newC, grid) &&
                            grid[newR][newC] != 0 &&
                            grid[newR][newC] != 2) {
                        q.offer(newR + "_" + newC);
                        // Rot the orange
                        grid[newR][newC] = 2;
                        convertedOranges++;
                    }
                }
            }
        }

        // Reduce the time by 1 as when the last set of oranges
        // are added to queue, then while loop runs one more time
        // thereby incrementing 'time' but none of the fresh oranges
        // are left to be added (since it was the last set). so, we
        // should discount this run cycle
        result[0] = --time;
        result[1] = convertedOranges;
        return result;
    }

    private static int[] getLocFromStr(String s) {
        int[] loc = new int[2];
        String[] strArr = s.split("_");
        loc[0] = Integer.parseInt(strArr[0]);
        loc[1] = Integer.parseInt(strArr[1]);
        return loc;
    }

    private static boolean isValid(int newR, int newC, int[][] grid) {
        if (newR < 0 || newR >= grid.length)
            return false;
        if (newC < 0 || newC >= grid[0].length)
            return false;
        return true;
    }

    public static void main(String[] args) {
        RottingOranges994 obj = new RottingOranges994();

        int[][] grid = {{2, 1, 1}, {1, 1, 0}, {0, 1, 1}};
        System.out.println("Result: " + obj.orangesRotting(grid)); // Output: 4

        grid = new int[][]{{2, 1, 1}, {0, 1, 1}, {1, 0, 1}};
        System.out.println("Result: " + obj.orangesRotting(grid)); // Output: -1

        grid = new int[][]{{0, 2}};
        System.out.println("Result: " + obj.orangesRotting(grid)); // Output: 0

        grid = new int[][]{{0}};
        System.out.println("Result: " + obj.orangesRotting(grid)); // Output: 0
    }
}
