package com.avi.leetcode;

public class MinimumPathSum64 {

    public int minPathSum(int[][] grid) {
        int rows = grid.length;
        int cols = grid[0].length;
        int[][] pathValueTable = new int[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int currentCellValue = grid[i][j];
                int leftIndex = j - 1;
                int upIndex = i - 1;
                int pathValue = 0;
                if (leftIndex < 0 && upIndex < 0) {
                    pathValue = currentCellValue;
                } else if (upIndex < 0) {
                    pathValue = pathValueTable[i][leftIndex] + currentCellValue;
                } else if (leftIndex < 0) {
                    pathValue = pathValueTable[upIndex][j] + currentCellValue;
                } else {
                    pathValue = Math.min(pathValueTable[i][leftIndex], pathValueTable[upIndex][j]) + currentCellValue;
                }
                pathValueTable[i][j] = pathValue;
            }
        }

        return pathValueTable[rows - 1][cols - 1];
    }

    public static void main(String[] args) {
        int[][] grid1 = {{1, 3, 1}, {1, 5, 1}, {4, 2, 1}};
        int[][] grid2 = {{1, 2, 3}, {4, 5, 6}};

        MinimumPathSum64 minPath = new MinimumPathSum64();
        int result = minPath.minPathSum(grid2);
        System.out.println("Minimum Path: " + result);
    }

}
