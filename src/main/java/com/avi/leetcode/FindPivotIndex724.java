package com.avi.leetcode;

public class FindPivotIndex724 {

    public int pivotIndex(int[] nums) {
        final int length = nums.length;
        int[] sumPerIndex = new int[length];

        // Creating progressive index sum
        int indexSum = 0;
        for (int i = 0; i < length; ++i) {
            indexSum = indexSum + nums[i];
            sumPerIndex[i] = indexSum;
        }
        //Utils.printArray(sumPerIndex);

        // Final comparison
        int pivotIndex = 0;
        boolean pivotSetFlag = false;
        for (int i = 0; i < length; ++i) {
            int leftSum = 0;
            int rightSum = sumPerIndex[length - 1] - sumPerIndex[i];
            if (i != 0) {
                leftSum = sumPerIndex[i - 1];
            }
            if (leftSum == rightSum) {
                pivotIndex = i;
                pivotSetFlag = true;
                break;
            }
        }
        return pivotSetFlag ? pivotIndex : -1;
    }


    public static void main(String[] args) {
        int[] input1 = {1, 7, 3, 6, 5, 6};
        int[] input2 = {1, 2, 3};
        int[] input3 = {2, 1, -1};

        FindPivotIndex724 pivotIndexObj = new FindPivotIndex724();
        int result = pivotIndexObj.pivotIndex(input3);
        System.out.println(result);
    }

}
