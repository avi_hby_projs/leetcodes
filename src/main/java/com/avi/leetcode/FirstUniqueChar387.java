package com.avi.leetcode;

public class FirstUniqueChar387 {

    public int firstUniqChar(String s) {
        final int len = s.length();
        int[] charMap = new int[26];
        int resultIndex = -1;

        for (int i = 0; i < len; ++i) {
            char currentChar = s.charAt(i);
            charMap[currentChar - 'a'] = charMap[currentChar - 'a'] + 1;
        }

        // We can iterate from index 0 to (len - 1) also. Just that we'll have to break after
        // we find the first unique character
        for (int i = len - 1; i >= 0; --i) {
            char currentChar = s.charAt(i);
            if (charMap[currentChar - 'a'] == 1) {
                resultIndex = i;
            }

        }
        return resultIndex;
    }

    public static void main(String[] args) {
        String input1 = "leetcode";
        String input2 = "loveleetcode";
        String input3 = "aabb";

        FirstUniqueChar387 firstUniqueObj = new FirstUniqueChar387();
        int result = firstUniqueObj.firstUniqChar(input1);
        System.out.println(result);
    }

}
