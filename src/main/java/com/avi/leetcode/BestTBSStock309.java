package com.avi.leetcode;

public class BestTBSStock309 {

    public int maxProfit(int[] prices) {
        // Check Pepcoding explaination: https://www.youtube.com/watch?v=GY0O57llkKQ
        // State machine:
        // Buy --> Sell --> Cooldown --+
        //  ^                          |
        //  |                          |
        //  +-------------------------+
        int maxProfitIfBoughtToday = -prices[0];
        int maxProfitIfSoldToday = 0;
        int cooldownPrice = 0;

        for (int i = 1; i < prices.length; i++) {
            int prevMaxProfitIfBoughtToday = maxProfitIfBoughtToday;
            maxProfitIfBoughtToday = Math.max(maxProfitIfBoughtToday, cooldownPrice - prices[i]);

            int prevMaxProfitIfSoldToday = maxProfitIfSoldToday;
            maxProfitIfSoldToday = Math.max(maxProfitIfSoldToday, prices[i] + prevMaxProfitIfBoughtToday);

            cooldownPrice = prevMaxProfitIfSoldToday;
        }

        return maxProfitIfSoldToday;
    }

    public static void main(String[] args) {
        int[] prices1 = {1,2,3,0,2};

        BestTBSStock309 obj = new BestTBSStock309();
        int result = obj.maxProfit(prices1);
        System.out.println("Result: " + result);
    }

}
