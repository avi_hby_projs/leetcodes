package com.avi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BalanceBST1382 {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public TreeNode balanceBST(TreeNode root) {
        List<Integer> arr = parseTreeToArray(root);
        Collections.sort(arr);

        TreeNode balTree = balancingFunction(arr, 0, arr.size() - 1);

        return balTree;
    }

    private List<Integer> parseTreeToArray(TreeNode root){
        List<Integer> list = new ArrayList<>();
        if(root == null)
            return list;
        list.add(root.val);
        List<Integer> leftList = parseTreeToArray(root.left);
        list.addAll(leftList);
        List<Integer> rightList = parseTreeToArray(root.right);
        list.addAll(rightList);
        return list;
    }

    private TreeNode balancingFunction(List<Integer> arr, int low, int high){
        if(low > high)
            return null;

        int midIdx = (low + high +1)/2;
        TreeNode node = new TreeNode(arr.get(midIdx));
        TreeNode left = balancingFunction(arr, low, midIdx - 1);
        TreeNode right = balancingFunction(arr, midIdx + 1, high);
        node.left = left;
        node.right = right;
        return node;
    }

    public static void main(String[] args) {
        String[] nums1 = {"1", "null", "2", "null", "3", "null", "4"};

        BalanceBST1382 obj = new BalanceBST1382();
        TreeNode result = obj.balanceBST(null);
        //System.out.println("Max K Sum pairs: " + result);
    }

}
