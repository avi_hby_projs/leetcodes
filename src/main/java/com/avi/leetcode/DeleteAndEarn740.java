package com.avi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DeleteAndEarn740 {

    public int deleteAndEarn(int[] nums) {

        Map<Integer, Integer> numMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer count = numMap.getOrDefault(nums[i], 0) + 1;
            numMap.put(nums[i], count);
        }

        int numsRemaining = nums.length;
        int score = 0;

        while (numsRemaining > 0) {
            int minDeleted = Integer.MAX_VALUE;
            int maxNum = 0;

            /*
            for (int i = 0; i < nums.length; i++) {
                int upNum = numMap.getOrDefault(nums[i] + 1, 0);
                int downNum = numMap.getOrDefault(nums[i] - 1, 0);
                int totalDeleted = upNum + downNum + 1;
                int num = nums[i];

                if (totalDeleted < minDeleted) {
                    minDeleted = totalDeleted;
                    maxNum = num;
                } else if (totalDeleted == minDeleted && num > maxNum) {
                    minDeleted = totalDeleted;
                    maxNum = num;
                }
            }*/

            for (Integer i : numMap.keySet()) {
                int upNum = numMap.getOrDefault(i + 1, 0);
                int downNum = numMap.getOrDefault(i - 1, 0);
                int totalDeleted = upNum + downNum + 1;
                int num = i;

                if (totalDeleted < minDeleted) {
                    minDeleted = totalDeleted;
                    maxNum = num;
                } else if (totalDeleted == minDeleted && num > maxNum) {
                    minDeleted = totalDeleted;
                    maxNum = num;
                }
            }

            // Delete nums
            numMap.remove(maxNum + 1);
            numMap.remove(maxNum - 1);
            Integer selectedNumNewCount = numMap.get(maxNum);
            if (selectedNumNewCount != null) {
                selectedNumNewCount = selectedNumNewCount - 1;
                if (selectedNumNewCount == 0)
                    numMap.remove(maxNum);
                else
                    numMap.put(maxNum, selectedNumNewCount);
            }
            score = score + maxNum;
            numsRemaining = numsRemaining - minDeleted;
        }

        return score;
    }

    public static void main(String[] args) {
        int[] nums1 = {2, 2, 3, 3, 3, 4};
        int[] nums2 = {3, 4, 2};

        DeleteAndEarn740 obj = new DeleteAndEarn740();
        int result = obj.deleteAndEarn(nums1);
        System.out.println("Result: " + result);
    }
}
