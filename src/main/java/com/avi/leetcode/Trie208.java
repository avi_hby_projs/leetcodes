package com.avi.leetcode;

public class Trie208 {

    private Node headNode;

    private class Node {
        public Node[] nextNode;
        public boolean isWord;

        public Node() {
            nextNode = new Node[26];
            isWord = false;
        }
    }

    private boolean nodeHasChar(Node node, char c) {
//        if (node != null && node.charArray[c - 'a'] == 1)
//            return true;
        return false;
    }

    public Trie208() {
        headNode = new Node();
    }

    // First complete this method
    public void insert(String word) {
        Node node = headNode;
        int i = 0;
        int wordLen = word.length();
        while (i < wordLen) {
            char c = word.charAt(i);
//            if (node == null) {
//                Node newNode = new Node();
////                node.nextNode = newNode;
////                node = newNode;
//            }
//            node.putChar(c);
//
//            // Mark last character as word complete
//            if (i == wordLen - 1)
//                node.isWord = true;
//            if (node.nextNode != null)
//                node = node.nextNode;
//            i++;
        }
    }

    // Copy from insert method and change
    public boolean search(String word) {
//        Node node = headNode.nextNode;
//        int i = 0;
//        int wordLen = word.length();
//        while (i < wordLen) {
//            char c = word.charAt(i);
//            if (!nodeHasChar(node, c)) {
//                return false;
//            }
//            // Check if last characters node is marked as word complete
//            if ((i == (wordLen - 1)) && node.isWord)
//                return true;
//            i++;
//        }
        return false;
    }

    // This is the one method where benefits of Trie over any other data structures become evident.
    // Copy from insert method and change
    public boolean startsWith(String prefix) {
//        Node node = headNode.nextNode;
//        int i = 0;
//        int wordLen = prefix.length();
//        while (i < wordLen) {
//            char c = prefix.charAt(i);
//            if (i == 0 && !nodeHasChar(node, c)) {
//                return false;
//            }
//            node = node.nextNode;
//            i++;
//        }
        return true;
    }

    public static void main(String[] args) {
        Trie208 t = new Trie208();
        t.insert("apple");
        boolean r = t.search("apple");
        System.out.println(r);
        r = t.search("app");
        System.out.println(r);
    }

}
