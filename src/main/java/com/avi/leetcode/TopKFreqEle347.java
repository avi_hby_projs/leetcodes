package com.avi.leetcode;

import com.avi.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TopKFreqEle347 {

    public int[] topKFrequent(int[] nums, int k) {

        Map<Integer, Integer> bucketMap = new HashMap<>();

        // Fill bucketMap with mapping: number -> occurance
        for (int i = 0; i < nums.length; i++) {
            bucketMap.put(nums[i], bucketMap.getOrDefault(nums[i], 0) + 1);
        }

        // Setting bucket array. The index of bucket array corresponds to the occurrence and the value
        // of the index denotes the number of that occurrence.
        // Benefit of this is that array is naturally sorted for occurrence.
        ArrayList<Integer>[] bucket = new ArrayList[nums.length + 1];
        for (Integer num : bucketMap.keySet()) {
            int occurrence = bucketMap.get(num);
            ArrayList<Integer> list = null;
            if (bucket[occurrence] == null) {
                list = new ArrayList<>();
                bucket[occurrence] = list;
            } else {
                list = bucket[occurrence];
            }
            list.add(num);
        }
        int[] result = new int[k];
        int count = 0;

        // Reverse traverse the bucket list to get highest occuring elements.
        for (int i = nums.length; i >= 0 && count < k; i--) {
            if (bucket[i] != null) {
                ArrayList<Integer> list = bucket[i];
                for (Integer item : list) {
                    result[count] = item;
                    ++count;
                    if (count >= k)
                        break;
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 1, 1, 2, 2, 3};
        int k1 = 2;
        int[] nums2 = {1};
        int k2 = 1;
        int[] nums3 = {1, 2};
        int k3 = 2;

        TopKFreqEle347 obj = new TopKFreqEle347();
        int[] result = obj.topKFrequent(nums1, k1);
        Utils.printArray(result);
    }
}
