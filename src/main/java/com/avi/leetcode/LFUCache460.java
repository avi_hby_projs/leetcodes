package com.avi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LFUCache460 {
    private int capacity;
    private int leastFreq;
    private Map<Integer, Node> map;
    private Map<Integer, LRUList> freqMap;

    class LRUList {
        private Node MRUNode;
        private Node LRUNode;
        private int size;

        public LRUList() {
            MRUNode = new Node(0, 0);
            LRUNode = new Node(0, 0);
            MRUNode.prev = LRUNode;
            LRUNode.next = MRUNode;
        }

        public void insertNode(Node node) {
            Node prev = MRUNode.prev;
            prev.next = node;
            node.prev = prev;
            node.next = MRUNode;
            MRUNode.prev = node;
            ++size;
        }

        public void removeNode(Node node) {
            Node nextNode = node.next;
            Node prevNode = node.prev;
            prevNode.next = nextNode;
            nextNode.prev = prevNode;
            node.next = null;
            node.prev = null;
            size--;
        }

        public void removeLRUNode() {
            Node node = LRUNode.next;
            removeNode(node);
            map.remove(node.key);
        }
    }

    class Node {
        private Node next;
        private Node prev;
        private int key;
        private int value;
        private int freq;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public LFUCache460(int capacity) {
        map = new HashMap<>();
        freqMap = new HashMap<>();
        this.capacity = capacity;
    }

    public int get(int key) {
        Node node = map.get(key);
        if (node == null)
            return -1;
        LRUList currList = freqMap.get(node.freq);
        currList.removeNode(node);
        if (currList.size == 0 && node.freq == leastFreq) {
            freqMap.remove(node.freq);
            ++leastFreq;
        }
        node.freq++;
        LRUList nextList = freqMap.get(node.freq);
        if (nextList == null) {
            nextList = new LRUList();
            nextList.insertNode(node);
            freqMap.put(node.freq, nextList);
        } else {
            nextList.insertNode(node);
        }
        return node.value;
    }

    public void put(int key, int value) {
        if (capacity == 0) return;

        Node node = map.get(key);
        if (node != null) {
            node.value = value;
            LRUList list = freqMap.get(node.freq);
            list.removeNode(node);
            if (list.size == 0 && node.freq == leastFreq) {
                freqMap.remove(node.freq);
                ++leastFreq;
            }
            node.freq++;
            LRUList nextlist = freqMap.get(node.freq);
            if (nextlist != null) {
                nextlist.insertNode(node);
            } else {
                LRUList newList = new LRUList();
                newList.insertNode(node);
                freqMap.put(node.freq, newList);
            }
        } else {
            // Evict node with least frequency
            if (map.size() + 1 > capacity) {
                LRUList lfulist = freqMap.get(leastFreq);
                lfulist.removeLRUNode();
                if (lfulist.size == 0) {
                    freqMap.remove(leastFreq);
                }
            }
            leastFreq = 1;

            node = new Node(key, value);
            node.freq = leastFreq;
            LRUList list = freqMap.get(node.freq);
            if (list != null)
                list.insertNode(node);
            else {
                list = new LRUList();
                list.insertNode(node);
                freqMap.put(node.freq, list);
            }
            map.put(key, node);
        }
    }

    public static void main(String[] args) {
        //["LFUCache", "put", "put", "get", "put",  "get", "get", "put", "get", "get", "get"]
        //[  [2],      [1, 1], [2, 2], [1],  [3, 3], [2],   [3],  [4, 4], [1],  [3],   [4]]
        LFUCache460 obj = new LFUCache460(2);
        obj.put(1, 1);
        obj.put(2, 2);
        obj.get(1);
        obj.put(3, 3);
        obj.get(2);
        obj.get(3);
        obj.put(4, 4);
        obj.get(1);
        obj.get(3);
        obj.get(4);
    }
}
