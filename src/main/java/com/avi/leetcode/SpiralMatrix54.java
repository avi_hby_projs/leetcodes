package com.avi.leetcode;

import java.util.ArrayList;
import java.util.List;

import static com.avi.Utils.printList;

public class SpiralMatrix54 {

    public List<Integer> spiralOrder(int[][] matrix) {
        int upperLim = 0;
        int lowerLim = matrix.length - 1;
        int leftLim = 0;
        int rightLim = matrix[0].length - 1;
        List<Integer> result = new ArrayList<>();

        while (upperLim <= lowerLim && leftLim <= rightLim) {

            // Left -> Right
            if (leftLim <= rightLim && upperLim <= lowerLim) {
                for (int i = leftLim; i <= rightLim; i++) {
                    result.add(matrix[upperLim][i]);
                }
                upperLim++;
            }

            // Up -> Down
            if (leftLim <= rightLim && upperLim <= lowerLim) {
                for (int i = upperLim; i <= lowerLim; i++) {
                    result.add(matrix[i][rightLim]);
                }
                rightLim--;
            }

            // Right -> Left
            if (leftLim <= rightLim && upperLim <= lowerLim) {
                for (int i = rightLim; i >= leftLim; i--) {
                    result.add(matrix[lowerLim][i]);
                }
                lowerLim--;
            }

            // Down -> Up
            if (leftLim <= rightLim && upperLim <= lowerLim) {
                for (int i = lowerLim; i >= upperLim; i--) {
                    result.add(matrix[i][leftLim]);
                }
                leftLim++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[][] matrix1 = new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        int[][] matrix2 = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        SpiralMatrix54 obj = new SpiralMatrix54();
        List<Integer> result = obj.spiralOrder(matrix2);
        printList(result);
    }

}
