package com.avi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordBreak139 {

    public boolean wordBreak(String s, List<String> wordDict) {
        // See Neetcode explaination: https://www.youtube.com/watch?v=Sx9NNgInc3A
        // The idea is to match the word from the end and set the arr[i] to true
        // only if previous word matched (using arr[i + word.length()]). In the end
        // if arr[0] is true then there is a continuously matching words in the
        // String s.
        int len = s.length();
        boolean[] arr = new boolean[len + 1];
        arr[len] = true;

        for (int i = len - 1; i >= 0; i--) {
            for (String word : wordDict) {
                if (word.length() + i <= len) {
                    String subStr = s.substring(i, i + word.length());
                    // Here it is important to check condition arr[i + word.length()] as,
                    // there might be other subsequent words in wordDict which might match
                    // but does not connect to previous matching word, leaving a gap
                    // between them.
                    if (subStr.equals(word) && arr[i + word.length()]) {
                        arr[i] = arr[i + word.length()];
                    }
                }
            }
        }

        return arr[0];
    }

    public static void main(String[] args) {
        String s1 = "applepenapple";
        String[] wd1 = {"apple", "pen"};
        List<String> wordDict1 = new ArrayList<>(Arrays.asList(wd1));

        WordBreak139 obj = new WordBreak139();
        boolean result = obj.wordBreak(s1, wordDict1);
        System.out.println("Word split: " + result);

        s1 = "abcd";
        wd1 = new String[]{"a", "abc", "b", "cd"};
        wordDict1 = new ArrayList<>(Arrays.asList(wd1));
        result = obj.wordBreak(s1, wordDict1);
        System.out.println("Word split: " + result);
    }
}
