package com.avi.leetcode;

public class CountTeams1395 {

    public int numTeams(int[] rating) {
        int count = 0;

        for (int i = 1; i < rating.length - 1; i++) {
            int leftSmaller = 0;
            int leftGreater = 0;
            int rightSmaller = 0;
            int rightGreater = 0;

            for (int j = 0; j < i; j++) {
                if (rating[j] < rating[i])
                    ++leftSmaller;
                else if (rating[j] > rating[i])
                    ++leftGreater;
            }

            for (int j = i + 1; j < rating.length; j++) {
                if (rating[j] > rating[i])
                    ++rightGreater;
                else if (rating[j] < rating[i])
                    ++rightSmaller;
            }

            count = count + (leftSmaller * rightGreater + leftGreater * rightSmaller);

        }

        return count;
    }

    public static void main(String[] args) {
        int[] rating1 = {2, 5, 3, 4, 1};

        CountTeams1395 obj = new CountTeams1395();
        int result = obj.numTeams(rating1);
        System.out.println("Result: " + result);
    }


}
