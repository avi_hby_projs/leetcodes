package com.avi.leetcode;

public class BestTBSStock123 {

    public int maxProfit(int[] prices) {
        // At each point in index we want to know max profit that can be made till that point and after that
        // point
        // Check Pepcoding explaination: https://www.youtube.com/watch?v=wuzTpONbd-0

        // Go through prices array from left to right and maintain min (buy) price and max profit that
        // can be made till index i by buying at that min price and selling at index i.
        int minPrice = prices[0];
        int[] profitLeftToRight = new int[prices.length];
        for (int i = 1; i < prices.length; i++) {
            minPrice = Math.min(minPrice, prices[i]);
            profitLeftToRight[i] = Math.max(profitLeftToRight[i - 1], prices[i] - minPrice);
        }

        // Go through prices array from right to left and maintain max (sell) price and max profit that
        // can be made till index i by buying at index i and selling at that max price.
        int maxPrice = prices[prices.length - 1];
        int[] profitRightToLeft = new int[prices.length];
        for (int i = prices.length - 2; i >= 0; i--) {
            maxPrice = Math.max(maxPrice, prices[i]);
            profitRightToLeft[i] = Math.max(profitRightToLeft[i + 1], maxPrice - prices[i]);
        }

        // Sum up the profit at each index and find max value
        int totalProfit = 0;
        for (int i = 0; i < prices.length; i++) {
            totalProfit = Math.max(totalProfit, profitLeftToRight[i] + profitRightToLeft[i]);
        }

        return totalProfit;
    }

    public static void main(String[] args) {
        int[] prices1 = {3, 3, 5, 0, 0, 3, 1, 4};
        int[] prices2 = {1, 2, 3, 4, 5};

        BestTBSStock123 obj = new BestTBSStock123();
        int result = obj.maxProfit(prices1);
        System.out.println("Result: " + result);
    }
}
