package com.avi.leetcode;

public class BestTBSStock188 {

    public int maxProfit(int k, int[] prices) {
        // No need for calculations for no values
        if (k == 0 || prices.length == 0)
            return 0;

        // Dynamic programming: Maintain an array of [0 -> k][0 -> prices.length].
        // Check Pepcoding explaination: https://www.youtube.com/watch?v=3YILP-PdEJA
        // Also check optimization: https://www.youtube.com/watch?v=3YILP-PdEJA&t=1966
        int[][] profitArray = new int[k + 1][prices.length];

        // i -> k : Transactions
        for (int i = 1; i <= k; i++) {
            // j -> prices.length : Day
            for (int j = 1; j < prices.length; j++) {
                // Calculate maximum profits if all i transactions have been done before day j
                int maxProfitAtKTransactions = profitArray[i][j - 1];
                int maxProfitAtKMinus1Transactions = 0;
                // Calculate max profit: if (k - 1) transaction has been done till day 0 and Kth transaction is done between day 0 (buy) and day j (sell)
                //                       if (k - 1) transaction has been done till day 1 and Kth transaction is done between day 1 (buy) and day j (sell)
                //                       if (k - 1) transaction has been done till day 2 and Kth transaction is done between day 2 (buy) and day j (sell)
                //                       if (k - 1) transaction has been done till day 3 and Kth transaction is done between day 3 (buy) and day j (sell)
                //                       . . . . . . . . . .
                //                       . . . . . . . . . .
                //                       if (k - 1) transaction has been done till day (j - 1) and Kth transaction is done between day j - 1 (buy) and day j (sell)
                for (int l = 0; l < j; l++) {
                    maxProfitAtKMinus1Transactions = Math.max(maxProfitAtKMinus1Transactions,
                            profitArray[i - 1][l] + prices[j] - prices[l]);
                }
                // Get max of Kth and (K - 1)th transaction
                profitArray[i][j] = Math.max(maxProfitAtKTransactions, maxProfitAtKMinus1Transactions);
            }
        }

        return profitArray[k][prices.length - 1];
    }

    public int maxProfitOptimized(int k, int[] prices) {

        int[][] grid = new int[k + 1][prices.length];

        // Optimization approach: https://www.youtube.com/watch?v=3YILP-PdEJA&t=1966

        for (int t = 1; t < k + 1; t++) {
            int maxInRow = grid[t - 1][0] - prices[0];
            for (int d = 1; d < prices.length; d++) {
                int noTransactionProfit = grid[t][d - 1];
                maxInRow = Math.max(maxInRow, grid[t - 1][d - 1] - prices[d - 1]);
                grid[t][d] = Math.max(noTransactionProfit, maxInRow + prices[d]);
            }
        }

        return grid[k][prices.length - 1];
    }

    public static void main(String[] args) {
        int[] prices1 = {3, 2, 6, 5, 0, 3};
        int k1 = 2;
        int[] prices2 = {6, 1, 3, 2, 4, 7};
        int k2 = 2;
        int[] prices3 = {1, 2, 4, 2, 5, 7, 2, 4, 9, 0};
        int k3 = 2;

        BestTBSStock188 obj = new BestTBSStock188();
        int result = obj.maxProfit(k1, prices1);
        System.out.println("Result: " + result);
    }
}
