package com.avi.leetcode;

public class HouseRobber198 {

    public int rob(int[] nums) {
        // See Pepcoding explaination: https://www.youtube.com/watch?v=VT4bZV24QNo
        // Use Dynamic Programming to store summation in case of two
        // options, include and exclude. You can either include a particular number,
        // In which case you add the current number to previous number's excluded
        // number plus current number. You can choose to exclude current number,
        // in which case, take the maximum of previously included number and previously
        // excluded number.
        int include = nums[0];
        int exclude = 0;

        for (int i = 1; i < nums.length; i++) {
            int includePrev = include;
            int excludePrev = exclude;

            // We will have to exclude previous number
            include = excludePrev + nums[i];
            // Take max of previously included sum or previously excluded number
            exclude = Math.max(includePrev, excludePrev);
        }

        return Math.max(include, exclude);
    }

    public static void main(String[] args) {
        HouseRobber198 obj = new HouseRobber198();

        int[] nums = {2, 7, 9, 3, 1};
        System.out.println("Result: " + obj.rob(nums)); // Output: 12

        nums = new int[]{2, 1, 1, 2};
        System.out.println("Result: " + obj.rob(nums)); // Output: 4
    }
}
