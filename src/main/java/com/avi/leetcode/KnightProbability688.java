package com.avi.leetcode;

public class KnightProbability688 {
    private int dimension;

    public double knightProbability(int n, int k, int row, int column) {
        this.dimension = n;
        // Start from curr array
        double[][] curr = new double[n][n];
        // Store calculations in next array
        double[][] next = new double[n][n];
        // Set probability of current position as 1. This will help in further calculations
        curr[row][column] = 1;
        // Direction vector for 8 possible moves
        int[][] directionVector = new int[][]{{-1, -2}, {-2, -1}, {-2, 1}, {-1, 2},
                {1, -2}, {2, -1}, {2, 1}, {1, 2}};

        // Make k moves
        for (int i = 0; i < k; i++) {
            // Go through each row and column
            for (int ro = 0; ro < n; ro++) {
                for (int col = 0; col < n; col++) {
                    // If prob > 0 in current position then calculate new positions from this position
                    if (curr[ro][col] > 0.0) {
                        // Calculate new positions from this position
                        for (int l = 0; l < 8; l++) {
                            int newRow = ro + directionVector[l][0];
                            int newCol = col + directionVector[l][1];
                            // If new position is valid then calculate new probability and set in next array
                            if (isValid(newRow, newCol)) {
                                // New probability = (curr[ro][col] / 8.0)
                                // Add this to next array
                                next[newRow][newCol] = next[newRow][newCol] + (curr[ro][col] / 8.0);
                            }
                        }
                    }
                }
            }
            // current can now be the next array as we will use a new next array for subsequent calculations
            curr = next;
            next = new double[n][n];
        }

        double p = 0;
        // Just sum up all the probabilities
        for (int ro = 0; ro < n; ro++) {
            for (int col = 0; col < n; col++) {
                p = p + curr[ro][col];
            }
        }

        return p;
    }

    private boolean isValid(int x, int y) {
        if (x >= 0 && y >= 0 && x < dimension && y < dimension)
            return true;
        return false;
    }

    public static void main(String[] args) {
        int n1 = 3;
        int k1 = 2;
        int row1 = 0;
        int column1 = 0;
        int n2 = 8;
        int k2 = 30;
        int row2 = 6;
        int column2 = 4;

        KnightProbability688 obj = new KnightProbability688();
        double result = obj.knightProbability(n2, k2, row2, column2);
        System.out.println("Result: " + result);
    }
}
