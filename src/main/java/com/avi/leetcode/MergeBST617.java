package com.avi.leetcode;

public class MergeBST617 {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    class Solution {
        public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
            // See Neetcode explaination: https://www.youtube.com/watch?v=QHH6rIK3dDQ
            // Perform DFS while combining the nodes of two trees

            if (root1 == null && root2 == null) {
                return null;
            }

            int root1Val = (root1 != null) ? root1.val : 0;
            int root2Val = (root2 != null) ? root2.val : 0;
            TreeNode mergedTree = new TreeNode(root1Val + root2Val);

            mergedTree.left = mergeTrees((root1 != null) ? root1.left : null, (root2 != null) ? root2.left : null);

            mergedTree.right = mergeTrees((root1 != null) ? root1.right : null, (root2 != null) ? root2.right : null);

            return mergedTree;
        }
    }
}
