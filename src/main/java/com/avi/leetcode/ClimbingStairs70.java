package com.avi.leetcode;

public class ClimbingStairs70 {
    public int climbStairs(int n) {
        if (n <= 0)
            return 0;
        if (n == 1)
            return 1;
        if (n == 2)
            return 2;

        int prev1 = 1;
        int prev2 = 2;
        int result = 0;
        for (int i = 2; i < n; i++) {
            result = prev1 + prev2;
            prev1 = prev2;
            prev2 = result;
        }

        return result;
    }

    public static void main(String[] args) {
        int n1 = 3;
        ClimbingStairs70 obj = new ClimbingStairs70();
        int result = obj.climbStairs(n1);
        System.out.println("Result: " + result);
    }


}
