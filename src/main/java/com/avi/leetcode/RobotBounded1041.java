package com.avi.leetcode;

public class RobotBounded1041 {

    public boolean isRobotBounded(String instructions) {
        boolean result = false;
        int[] initialPos = new int[]{0, 0};
        int[] currPos = new int[]{0, 0};
        int directionIdx = 0;
        int[][] directionVector = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < instructions.length(); j++) {
                int currX = currPos[0];
                int currY = currPos[1];
                char instruction = instructions.charAt(j);
                if (instruction == 'G') {
                    currX = currX + directionVector[directionIdx][0];
                    currY = currY + directionVector[directionIdx][1];
                } else if (instruction == 'R') {
                    directionIdx++;
                    directionIdx = directionIdx % 4;
                } else if (instruction == 'L') {
                    directionIdx--;
                    if (directionIdx < 0)
                        directionIdx = 3;
                }

                currPos[0] = currX;
                currPos[1] = currY;
            }

            // Check if returned to initial position after 2 or 4 iterations
            if ((i + 1) % 2 == 0 && currPos[0] == initialPos[0] && currPos[1] == initialPos[1]) {
                return true;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        String instructions1 = "GGLLGG";
        String instructions2 = "GG";

        RobotBounded1041 sim = new RobotBounded1041();
        boolean result = sim.isRobotBounded(instructions2);
        System.out.println("Is Robot Bounded: " + result);
    }
}
