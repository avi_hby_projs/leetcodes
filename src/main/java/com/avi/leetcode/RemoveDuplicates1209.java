package com.avi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class RemoveDuplicates1209 {

    public String removeDuplicates(String s, int k) {
        int pointerIdx = -1;
        List<Character> lList = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (pointerIdx == -1) {
                lList.add(c);
                ++pointerIdx;
            } else {
                if (!lList.get(lList.size() - 1).equals(c)) {
                    pointerIdx = lList.size();
                }
                lList.add(c);

                if (lList.size() - pointerIdx == k) {
                    lList.subList(pointerIdx, lList.size()).clear();
                    pointerIdx--;

                    // Pushback pointer to the first instance of the character
                    if (lList.size() > 0) {
                        char topChar = lList.get(lList.size() - 1);
                        while (pointerIdx > 0 && lList.get(pointerIdx - 1) == topChar) {
                            pointerIdx--;
                        }
                    }
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        for (Character c : lList) {
            sb.append(c);
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        String s1 = "deeedbbcccbdaa";
        int k1 = 3;
        String s2 = "abcd";
        int k2 = 2;
        String s3 = "pbbcggttciiippooaais";
        int k3 = 2;
        String s4 = "ghanyhhhhhttttttthhyyyyyynnnnnnyqkkkkkkkrrrrrrjjjjjjjryyyyyyfffffffygq"; // Output "ghayqgq"
        int k4 = 7;

        RemoveDuplicates1209 obj = new RemoveDuplicates1209();
        String result = obj.removeDuplicates(s1, k1);
        System.out.println("Reduced String: " + result);
    }
}
