package com.avi.leetcode;

import java.util.*;

public class WordSearch79 {

    public boolean exist(char[][] board, String word) {

        // Notes:
        // 1) Here BFS will not work as we need to check a particular sequence of words. In BFS we just traverse
        // the adjoining nodes without having a particular sequence check in place. However, in DFS we can check the
        // seqence of word appearing in the variable 'word'.
        // 2) Here, pure DFS will also not work as we might need to visit the same node again, which is
        // not the case with pure DFS.
        // 3) Use Backtracking in this case which is a special case of DFS in which we visit the
        // same node again, but in different configuration.
        // 4) BFS              -> Queue
        //    DFS              -> Stack
        //    Backtracking DFS -> Recursion (DFS using stack is also possible but very tricky)
        // 5) In Java Queue is an interface while S tack is a class (implemented using Vector data structure). LinkedList
        // can also be used to implement Stack.

        int rows = board.length;
        int cols = board[0].length;
        Set<String> visitSet = new HashSet<>();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                char currChar = word.charAt(0);
                if (board[i][j] == currChar) {
                    if (doDFSBacktracking(board, i, j, word, 0, visitSet))
                        return true;
                }
            }
        }

        return false;
    }

    private boolean doDFSBacktracking(char[][] board, int i, int j, String word, int wordIdx, Set<String> visitSet) {
        if (i >= 0 && i < board.length && j >= 0 && j < board[0].length &&
                wordIdx < word.length() && word.charAt(wordIdx) == board[i][j] && !visitSet.contains(i+"_"+ j)) {
            if (word.length() == (wordIdx + 1))
                return true;
            visitSet.add(i + "_" + j);
        } else {
            return false;
        }

        if (doDFSBacktracking(board, i, j + 1, word, wordIdx + 1, visitSet) ||
                doDFSBacktracking(board, i + 1, j, word, wordIdx + 1, visitSet) ||
                doDFSBacktracking(board, i, j - 1, word, wordIdx + 1, visitSet) ||
                doDFSBacktracking(board, i - 1, j, word, wordIdx + 1, visitSet)) {
            return true;
        }

        visitSet.remove(i + "_" + j);

        return false;
    }

    public static void main(String[] args) {
        WordSearch79 obj = new WordSearch79();
        char[][] board = {{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}};
        String word = "SEE";
        System.out.println("Word Present: " + obj.exist(board, word)); // Output: true

        board = new char[][]{{'a', 'a'}};
        word = "a";
        System.out.println("Word Present: " + obj.exist(board, word)); // Output: true

        board = new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'E', 'S'}, {'A', 'D', 'E', 'E'}};
        word = "ABCESEEEFS";
        System.out.println("Word Present: " + obj.exist(board, word)); // Output: true

        board = new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'E', 'S'}, {'A', 'D', 'E', 'E'}};
        word = "ABCEFSADEESE";
        System.out.println("Word Present: " + obj.exist(board, word)); // Output: true

        board = new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}};
        word = "ABCB";
        System.out.println("Word Present: " + obj.exist(board, word)); // Output: false

    }
}
