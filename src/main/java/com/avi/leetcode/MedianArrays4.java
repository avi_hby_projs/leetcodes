package com.avi.leetcode;

public class MedianArrays4 {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        // {1, 2}
        // {3, 4}

        // {1, 2, 3}
        // {4, 5}

        // Even - Avg of two mids
        // Odd - Mid

        if(nums1.length > nums2.length)
            return findMedianSortedArrays(nums2, nums1);

        boolean isEven = (nums1.length + nums2.length) % 2 == 0;

        // {1, 2, 3}
        // {}
        if (nums1.length == 0) {
            if (isEven)
                return (nums2[nums2.length / 2] + nums2[(nums2.length / 2) - 1]) / 2.0;
            else
                return nums2[nums2.length / 2];
        }
        if (nums2.length == 0) {
            if (isEven)
                return (nums1[nums1.length / 2] + nums1[(nums1.length / 2) - 1]) / 2.0;
            else
                return nums1[nums1.length / 2];
        }

        int low = 0;
        int high = nums1.length;
        int leftPartSize = (nums1.length + nums2.length + 1) / 2;

        while (low <= high) {
            int mid1 = (low + high) / 2;
            int mid2 = leftPartSize - mid1;

            int leftMid1 = (mid1 == 0) ? Integer.MIN_VALUE : nums1[mid1 - 1];
            int rightMid1 = (mid1 == nums1.length) ? Integer.MAX_VALUE : nums1[mid1];
            int leftMid2 = (mid2 == 0) ? Integer.MIN_VALUE : nums2[mid2 - 1];
            int rightMid2 = (mid2 == nums2.length) ? Integer.MAX_VALUE : nums2[mid2];


            if (leftMid1 <= rightMid2 && leftMid2 <= rightMid1) {
                if (isEven)
                    return (Math.max(leftMid1, leftMid2) + Math.min(rightMid1, rightMid2)) / 2.0;
                else
                    return Math.max(leftMid1, leftMid2);
            } else if (leftMid1 > rightMid2) {
                high = mid1 - 1;
            } else {
                low = mid1 + 1;
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        int[] nums11 = {1, 3};
        int[] nums21 = {2};
        int[] nums12 = {1, 2};
        int[] nums22 = {3, 4};
        int[] nums13 = {1,2,3,5,6};
        int[] nums23 = {4};

        MedianArrays4 obj = new MedianArrays4();
        double result = obj.findMedianSortedArrays(nums13, nums23);
        System.out.println("Median: " + result);
    }
}
