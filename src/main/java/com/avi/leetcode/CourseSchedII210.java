package com.avi.leetcode;

import java.util.*;

public class CourseSchedII210 {
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        // See Neetcode explaination: https://www.youtube.com/watch?v=Akt3glAwyfY
        // Perform topological sort using DFS on map. Convert prerequisites array into
        // adjacency list to represent map. Don't forget to detect cycles in the map.

        Map<Integer, List<Integer>> map = new HashMap<>();

        // Create adjacency list for each course item
        for (int i = 0; i < numCourses; i++) {
            List<Integer> list = new LinkedList<>();
            map.put(i, list);
        }

        // Fill adjacency list using prerequisites array
        for (int i = 0; i < prerequisites.length; i++) {
            List<Integer> list;
            if (map.containsKey(prerequisites[i][1])) {
                list = map.get(prerequisites[i][1]);
                list.add(prerequisites[i][0]);
            }
        }

        List<Integer> seqList = new LinkedList<>();
        Set<Integer> visitedSet = new HashSet<>();

        // Perform DFS on each adjacency list
        for (Integer i : map.keySet()) {
            boolean hasCycle = dfs(i, map, seqList, visitedSet);
            if (hasCycle) {
                seqList.clear();
                break;
            }
        }

        // If no sequence found or cycle is detected then just
        // fill the array with courses
        if (seqList.isEmpty()) {
            int[] result = new int[numCourses];
            for (int i = 0; i < numCourses; i++) {
                result[i] = i;
            }
            return result;
        }

        // Reverse the List (this can be avoided while using specific
        // sequence while filling up adjacency list using prerequisites array)
        int[] result = new int[seqList.size()];
        for (int i = seqList.size() - 1; i >= 0; i--) {
            result[seqList.size() - 1 - i] = seqList.get(i);
        }

        return result;
    }

    private boolean dfs(Integer i, Map<Integer, List<Integer>> map, List<Integer> seqList,
                        Set<Integer> vistedSet) {
        Stack<Integer> stack = new Stack<>();
        // Keep a set that stores the items in stack to detect cycle in the
        // map. Cycle is detected when same item is to be added into the
        // stack which is already present in the stack.
        Set<Integer> stackSet = new HashSet<>();
        if (!vistedSet.contains(i)) {
            stack.push(i);
            stackSet.add(i);
        }

        while (!stack.isEmpty()) {
            Integer k = stack.peek();
            if (map.containsKey(k)) {
                List<Integer> l = map.get(k);
                boolean noItems = true;
                for (Integer j : l) {
                    if (!vistedSet.contains(j)) {
                        // Detect cycle
                        if (stackSet.contains(j)) {
                            return true;
                        }
                        stack.push(j);
                        stackSet.add(j);
                        noItems = false;
                        break;
                    }
                }
                if (noItems) {
                    Integer m = stack.pop();
                    stackSet.remove(m);
                    vistedSet.add(m);
                    seqList.add(m);
                }
            } else {
                Integer m = stack.pop();
                stackSet.remove(m);
                vistedSet.add(m);
                seqList.add(m);
            }
        }

        return false;
    }

    public static void main(String[] args) {
        CourseSchedII210 obj = new CourseSchedII210();

        int numCourses = 2;
        int[][] prerequisites = {{1, 0}};
        System.out.println(obj.findOrder(numCourses, prerequisites)); // [0,1]

        numCourses = 4;
        prerequisites = new int[][]{{1, 0}, {2, 0}, {3, 1}, {3, 2}};
        System.out.println(obj.findOrder(numCourses, prerequisites)); // [0,2,1,3]

        numCourses = 1;
        prerequisites = new int[][]{};
        System.out.println(obj.findOrder(numCourses, prerequisites)); // [0]

        numCourses = 2;
        prerequisites = new int[][]{};
        System.out.println(obj.findOrder(numCourses, prerequisites)); // [1,0]

        numCourses = 2;
        prerequisites = new int[][]{{0, 1}, {1, 0}};
        System.out.println(obj.findOrder(numCourses, prerequisites)); // []

        numCourses = 4;
        prerequisites = new int[][]{{2, 0}, {1, 0}, {3, 1}, {3, 2}, {1, 3}};
        System.out.println(obj.findOrder(numCourses, prerequisites)); // []
    }
}
