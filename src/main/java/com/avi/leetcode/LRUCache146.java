package com.avi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LRUCache146 {

    class Node {
        private Node next;
        private Node prev;
        private int key;
        private int value;

        public Node getNext() {
            return this.next;
        }

        public void setNext(Node node) {
            this.next = node;
        }

        public Node getPrev() {
            return this.prev;
        }

        public void setPrev(Node node) {
            this.prev = node;
        }

        public int getValue() {
            return this.value;
        }

        public void setValue(int val) {
            this.value = val;
        }

        public int getKey() {
            return this.key;
        }

        public void setKey(int key) {
            this.key = key;
        }

    }

    private int capacity;
    private Map<Integer, Node> map;
    private Node MRUNode;
    private Node LRUNode;

    public LRUCache146(int capacity) {
        map = new HashMap<>();
        this.capacity = capacity;
        MRUNode = new Node();
        LRUNode = new Node();
        MRUNode.prev = LRUNode;
        LRUNode.next = MRUNode;
    }

    public int get(int key) {
        System.out.println("Getting: " + key);
        printState();
        if (map.containsKey(key)) {
            Node existingNode = map.get(key);
            if (existingNode.getNext() != MRUNode) {
                removeNode(existingNode);
                insertNode(existingNode);
            }
            return existingNode.getValue();
        }

        return -1;
    }

    public void put(int key, int value) {
        System.out.println("Inserting: " + key + " => " + value);
        printState();
        // Add node
        if (map.containsKey(key)) {
            Node existingNode = map.get(key);
            existingNode.setValue(value);
            if (existingNode.getNext() != MRUNode) {
                removeNode(existingNode);
                insertNode(existingNode);
            }
        } else if (map.size() < capacity) {
            Node node = new Node();
            node.setKey(key);
            node.setValue(value);
            map.put(key, node);
            insertNode(node);
        } else {

            // Evict
            Node lruNode = LRUNode.getNext();
            System.out.println("Evicting: " + lruNode.getKey() + " => " + lruNode.getValue());
            removeNode(lruNode);
            map.remove(lruNode.getKey());
            // Add node
            Node node = new Node();
            node.setKey(key);
            node.setValue(value);
            map.put(key, node);
            insertNode(node);
        }
        printState();
    }

    private void insertNode(Node node) {
        Node prevNode = MRUNode.getPrev();
        prevNode.setNext(node);
        node.setNext(MRUNode);
        node.setPrev(prevNode);
        MRUNode.setPrev(node);
    }

    private void removeNode(Node node) {
        Node nextNode = node.getNext();
        Node prevNode = node.getPrev();
        prevNode.setNext(nextNode);
        nextNode.setPrev(prevNode);
        node.setNext(null);
        node.setPrev(null);
    }

    private void printState() {
        System.out.print("LL: ");
        Node n = MRUNode.getPrev();
        while (n.getPrev() != null) {
            System.out.print("[" + n.getKey() + ", " + n.getValue() + "] => ");
            n = n.getPrev();
        }
        System.out.print("\nMap: ");
        for (Integer i : map.keySet()) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        LRUCache146 obj = new LRUCache146(2);
        obj.put(1, 1);
        obj.put(2, 2);
        obj.get(1);

    }
}
