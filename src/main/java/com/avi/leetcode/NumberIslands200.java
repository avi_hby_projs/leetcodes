package com.avi.leetcode;

import java.util.*;

public class NumberIslands200 {
    public int numIslands(char[][] grid) {
        int islandCount = 0;
        int rows = grid.length;
        int cols = grid[0].length;
        int[][] directionVector = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};
        Set<String> visited = new HashSet<>();
        Queue<String> procQ = new LinkedList<>();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == '1' && !visited.contains(i + "," + j)) {
                    islandCount++;
                    procQ.offer(i + "," + j);
                    // Process element
                    while (!procQ.isEmpty()) {
                        String idxKey = procQ.poll();
                        int[] idx = getIndexFromStringKey(idxKey);
                        char element = grid[idx[0]][idx[1]];
                        if (element == '1' && !visited.contains(idxKey)) {
                            visited.add(idxKey);
                            getNextElements(idx, rows, cols, directionVector, procQ);
                        }
                    }
                }
            }
        }

        return islandCount;
    }

    private void getNextElements(int[] idx, int rows, int cols, int[][] directionVector, Queue<String> procQ) {
        for (int i = 0; i < 4; i++) {
            int newX = idx[0] + directionVector[i][0];
            int newY = idx[1] + directionVector[i][1];
            if (newX >= 0 && newX < rows && newY >= 0 && newY < cols) {
                procQ.offer(newX + "," + newY);
            }
        }
    }

    private int[] getIndexFromStringKey(String poll) {
        String[] key = poll.split(",");
        int[] idx = new int[2];
        idx[0] = Integer.valueOf(key[0]);
        idx[1] = Integer.valueOf(key[1]);
        return idx;
    }

    public static void main(String[] args) {
        char[][] grid1 = {
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'}};
        char[][] grid2 = {
                {'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}};

        NumberIslands200 obj = new NumberIslands200();
        int result = obj.numIslands(grid2);
        System.out.println("Number of Islands: " + result);
    }
}
