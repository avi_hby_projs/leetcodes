package com.avi.leetcode;

public class TrapRainWater42 {
    public int trap(int[] height) {
        // Similar approach as BestTBSStock121
        int waterQuantity = 0;
        int[] maxLeft = new int[height.length];
        int[] maxRight = new int[height.length];

        for (int i = 0; i < height.length; ++i) {
            if (i == 0)
                maxLeft[i] = 0;
            else {
                if (height[i - 1] > maxLeft[i - 1])
                    maxLeft[i] = height[i - 1];
                else
                    maxLeft[i] = maxLeft[i - 1];
            }
        }

        for (int i = height.length - 1; i >= 0; --i) {
            if (i == height.length - 1)
                maxRight[i] = 0;
            else {
                if (height[i + 1] > maxRight[i + 1])
                    maxRight[i] = height[i + 1];
                else
                    maxRight[i] = maxRight[i + 1];
            }
        }

        for (int i = 0; i < height.length; ++i) {
            int q = Math.min(maxLeft[i], maxRight[i]) - height[i];
            if (q > 0)
                waterQuantity = waterQuantity + q;
        }

        return waterQuantity;
    }

    public static void main(String[] args) {
        int[] height1 = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};

        TrapRainWater42 obj = new TrapRainWater42();
        int result = obj.trap(height1);
        System.out.println("Result: " + result);
    }
}
