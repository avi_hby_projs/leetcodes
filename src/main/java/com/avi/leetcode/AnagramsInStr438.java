package com.avi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.avi.Utils.printList;

public class AnagramsInStr438 {

    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> op = new ArrayList<>();

        if (p.length() > s.length())
            return op;

        Map<Character, Integer> pMap = new HashMap<>();
        Map<Character, Integer> sMap = new HashMap<>();

        // Fill pMap
        for (int i = 0; i < p.length(); i++) {
            pMap.put(p.charAt(i), pMap.getOrDefault(p.charAt(i), 0) + 1);
        }

        // Analyze s with pMap
        int i = 0;
        int j = p.length() - 1;

        // If sMap is empty then fill it
        for (int k = 0; k <= j; k++) {
            sMap.put(s.charAt(k), sMap.getOrDefault(s.charAt(k), 0) + 1);
        }

        while (j < s.length()) {
            int matchCount = 0;
            // Compare pMap with sMap
            for (Character pC : pMap.keySet()) {
                if (sMap.containsKey(pC)) {
                    int sCount = sMap.get(pC);
                    int pCount = pMap.get(pC);
                    if (sCount == pCount)
                        matchCount = matchCount + sCount;
                }
            }

            if (matchCount == p.length()) {
                op.add(i);
            }

            // Increment pointers
            Character removeChar = s.charAt(i);
            sMap.put(removeChar, sMap.get(removeChar) - 1);
            if (sMap.get(removeChar) == 0)
                sMap.remove(removeChar);
            ++i;
            ++j;
            if (j < s.length())
                sMap.put(s.charAt(j), sMap.getOrDefault(s.charAt(j), 0) + 1);
        }

        return op;
    }

    public static void main(String[] args) {
        String s1 = "cbaebabacd";
        String p1 = "abc";
        String s2 = "baa";
        String p2 = "aa";
        AnagramsInStr438 ana = new AnagramsInStr438();
        List<Integer> op = ana.findAnagrams(s1, p1);
        printList(op);
    }

}
