package com.avi.leetcode;

import java.util.*;

import static com.avi.Utils.printListsOfList;

public class NQueens51 {
    private List<List<String>> result;
    private Set<Integer> invalidCol;
    private Set<Integer> invalidDiag1;
    private Set<Integer> invalidDiag2;
    private int dimension;

    public List<List<String>> solveNQueens(int n) {
        this.dimension = n;
        result = new ArrayList<>();
        invalidCol = new HashSet<>();
        invalidDiag1 = new HashSet<>();
        invalidDiag2 = new HashSet<>();
        boardCombinations(new ArrayList<>(), 0);

        return result;
    }

    private void boardCombinations(List<String> queenLocation, int row) {
        // Only add list to result when we have successfully parsed all rows and columns
        if (row == dimension) {
            // Copy locations to new list since we will be updating queenLocation while backtracking
            result.add(new ArrayList<>(queenLocation));
            return;
        }

        // Parse all columns for this particular row
        for (int col = 0; col < dimension; col++) {
            if (invalidCol.contains(col) || invalidDiag1.contains(row + col) || invalidDiag2.contains(row - col)) {
                continue;
            }

            // Set the queen location
            char[] arr = new char[dimension];
            Arrays.fill(arr, '.');
            arr[col] = 'Q';
            String s = new String(arr);
            queenLocation.add(s);

            // Set new invalid positions based on above queen's location
            invalidCol.add(col);
            invalidDiag1.add(row + col);
            invalidDiag2.add(row - col);

            // Now based on currently set queen's location try combinations for next row
            boardCombinations(queenLocation, row + 1);

            // Backtrack invalid positions to try new combinations
            queenLocation.remove(queenLocation.size() - 1);
            invalidCol.remove(col);
            invalidDiag1.remove(row + col);
            invalidDiag2.remove(row - col);
        }
    }

    public static void main(String[] args) {
        int n1 = 4;

        NQueens51 obj = new NQueens51();
        List<List<String>> result = obj.solveNQueens(n1);
        printListsOfList(result);
    }
}
