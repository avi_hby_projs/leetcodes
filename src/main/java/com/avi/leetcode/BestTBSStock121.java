package com.avi.leetcode;

public class BestTBSStock121 {

    public int maxProfit(int[] prices) {
        // Can be done with similar state machine technique as LC 309 Best Time to Buy and Sell Stock with Cooldown
        // Check Pepcoding explaination: https://www.youtube.com/watch?v=GY0O57llkKQ
        int maxProfitIfBoughtToday = -prices[0];
        int maxProfitIfSoldToday = 0;

        for (int i = 1; i < prices.length; i++) {
            maxProfitIfSoldToday = Math.max(maxProfitIfSoldToday, prices[i] + maxProfitIfBoughtToday);
            maxProfitIfBoughtToday = Math.max(maxProfitIfBoughtToday, -prices[i]);
        }

        return maxProfitIfSoldToday;
    }

    private int maxProfit2(int[] prices) {
        // Simple approach
        int[] minArr = new int[prices.length];
        int[] maxArr = new int[prices.length];

        // Similar approach as TrapRainWater42
        // Min array from left
        minArr[0] = prices[0];
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] < minArr[i - 1]) {
                minArr[i] = prices[i];
            } else {
                minArr[i] = minArr[i - 1];
            }
        }

        // Max array from right
        maxArr[prices.length - 1] = prices[prices.length - 1];
        for (int i = prices.length - 2; i >= 0; i--) {
            if (prices[i] > maxArr[i + 1]) {
                maxArr[i] = prices[i];
            } else {
                maxArr[i] = maxArr[i + 1];
            }
        }
        int maxProfit = 0;

        // Profit checking by difference of max and min on each day
        for (int i = 0; i < prices.length; i++) {
            int profit = maxArr[i] - minArr[i];
            if (profit > maxProfit)
                maxProfit = profit;
        }

        return maxProfit;
    }

    public static void main(String[] args) {
        int[] prices1 = {7, 1, 5, 3, 6, 4};
        int[] prices2 = {7, 6, 4, 3, 1};

        BestTBSStock121 obj = new BestTBSStock121();
        int result = obj.maxProfit(prices1);
        System.out.println("Result: " + result);
    }

}
